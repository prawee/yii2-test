<?php

namespace test;

class Module extends \yii\base\Module
{
    public $controllerNamespace='test\controllers';
    public function init()
    {
        parent::init();
        $this->setAliases(['@test'=>__DIR__]);
    }
}
